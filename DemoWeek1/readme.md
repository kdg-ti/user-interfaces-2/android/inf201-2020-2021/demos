Demo
====
- build.gradle: twee versies
- AndroidManifest.xml
- onder module app: 
  - java folder bevat Kotlin
  - res folder bevat resources --> wordt automatisch R klasse gegenereerd om de resources aan te kunnen spreken
- Activity: 
  - scherm van je app, erven van AppCompatActivity
  - minstens onCreate overschrijven
  - gebruik findViewById om views uit de layout op te halen
- Layout
  - xml file die UI beschrijft
  - in de onCreate van de Activity wordt de Activity verbonden met deze layout via setContentview  
  - verschillende layoutmanagers mogelijk, de meest flexibele is ConstraintLayout
  - geef de verschillende view in de layout een zinvolle id
- strings.xml
  - in de res/values folder
  - bevat alle strings van je applicatie
- Kotlin:
  - methodes: fun keyword
  - override: geen annotatie, wel keyword
  - types:
    - static typed
    - niet nodig te declareren als het type uit de context kan afgeleid worden
    - ? voor nullable types
   


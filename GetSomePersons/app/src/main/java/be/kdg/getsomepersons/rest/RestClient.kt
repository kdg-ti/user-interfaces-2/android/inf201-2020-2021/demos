package be.kdg.getsomepersons.rest

import be.kdg.getsomepersons.model.Person
import com.google.gson.GsonBuilder
import io.reactivex.rxjava3.core.Observable
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

const val BASE_URL = "http://10.0.2.2:3000"

class RestClient {

    private fun connect(pad: String, method: String = "GET"): HttpURLConnection {
        val url = URL(BASE_URL + pad)
        val connection = url.openConnection() as HttpURLConnection
        connection.apply {
            connectTimeout = 15000
            readTimeout = 10000
            requestMethod = method
            connect()
        }
        return connection
    }

    fun getPerson(id: Int): Observable<Person> {
        return Observable.create<Person>{emitter ->
            try {
                val connection = connect("/persons/${id}")
                val gson = GsonBuilder().create()
                val person =
                    gson.fromJson(InputStreamReader(connection.inputStream), Person::class.java)
                emitter.onNext(person)
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
    }

    //voor alle restcalls een functie
}
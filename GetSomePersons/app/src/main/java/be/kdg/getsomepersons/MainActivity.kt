package be.kdg.getsomepersons

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import be.kdg.getsomepersons.databinding.ActivityMainBinding
import be.kdg.getsomepersons.rest.RestClient
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater);
        setContentView(binding.root)
        RestClient().getPerson(1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
            binding.idHelloWorld.text = it.givenName
        }, {
            Toast.makeText(this@MainActivity, "Probleem:${it.message}", Toast.LENGTH_LONG)
                .show()
        })
    }
}
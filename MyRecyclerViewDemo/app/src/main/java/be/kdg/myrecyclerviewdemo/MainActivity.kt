package be.kdg.myrecyclerviewdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.kdg.myrecyclerviewdemo.adapters.ContactsAdapter
import be.kdg.myrecyclerviewdemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), ContactsAdapter.ContactsListener {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.rvContacts.adapter = ContactsAdapter(this);
        binding.rvContacts.layoutManager = LinearLayoutManager(this);
    }

    override fun contactSelected(index: Int) {
        TODO("Not yet implemented")
    }
}